KOSapi-Splus integration library (retired)
=================================

This project is obsoleted by https://gitlab.fit.cvut.cz/dobryjak/spluskos

About
-----

This project (library) is capable of loading timetable-related data from [KOSapi-3](https://kosapi.fit.cvut.cz/) and saving them into a SQL database. The purpose is loading the Scientia Syllabus Plus course plannig application with data from KOS IS.

License
-------

Copyright Ondřej Guth, 2019, [Faculty of Information Technology](https://fit.cvut.cz) of [Czech Technical University in Prague](https://www.cvut.cz/).
