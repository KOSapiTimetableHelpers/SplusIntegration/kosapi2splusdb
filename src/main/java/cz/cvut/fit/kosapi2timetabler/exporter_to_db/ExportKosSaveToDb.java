package cz.cvut.fit.kosapi2timetabler.exporter_to_db;

import cz.cvut.fit.kosapi2timetabler.dto.Course;
import cz.cvut.fit.kosapi2timetabler.dto.Parallel;
import cz.cvut.fit.kosapi2timetabler.dto.Person;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal.*;
import cz.cvut.fit.kosapi2timetabler.timetabler_db.*;
import cz.cvut.fit.kosapi2timetabler.timetabler_db.Module;

import javax.persistence.EntityManager;
import javax.xml.bind.UnmarshalException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.DayOfWeek;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExportKosSaveToDb {
	private static final ActivityType LABORATORY_ACTIVITY_TYPE = new ActivityType("lab");
	private static final ActivityType SEMINAR_ACTIVITY_TYPE = new ActivityType("sem");
	private static final ActivityType LECTURE_ACTIVITY_TYPE = new ActivityType("lec");
	private static final Map<String, ActivityType> ACTIVITY_TYPES = Map.of(
			"LECTURE", LECTURE_ACTIVITY_TYPE,
			"TUTORIAL", SEMINAR_ACTIVITY_TYPE,
			"LABORATORY", LABORATORY_ACTIVITY_TYPE);
	private static final String WEEK_AVAILABILITY_BOTH_LABEL = "BOTH";
	private static final String WEEK_AVAILABILITY_EVEN_LABEL = "EVEN";
	private static final String WEEK_AVAILABILITY_ODD_LABEL = "ODD";

	private final Map<String, Availability> weekAvailabilities = new HashMap<>();
	private transient final EntityManager entityManager;
	private final String semesterCode;
	private transient final Map<String, Teacher> username2teacher = new HashMap<>();
	private transient final TeacherDao teacherDao = new TeacherDao();
	private transient final ParallelDao parallelDao;
	private transient final CourseDao courseDao;

	public ExportKosSaveToDb(final String weekMaskBoth,
	                         final String weekMaskEven,
	                         final String weekMaskOdd,
	                         final EntityManager em,
	                         final String semCode) {
		weekAvailabilities.put(WEEK_AVAILABILITY_BOTH_LABEL, new Availability(WEEK_AVAILABILITY_BOTH_LABEL, weekMaskBoth));
		weekAvailabilities.put(WEEK_AVAILABILITY_EVEN_LABEL, new Availability(WEEK_AVAILABILITY_EVEN_LABEL, weekMaskEven));
		weekAvailabilities.put(WEEK_AVAILABILITY_ODD_LABEL, new Availability(WEEK_AVAILABILITY_ODD_LABEL, weekMaskOdd));
		entityManager = em;
		semesterCode = semCode;
		courseDao = new CourseDao(semesterCode);
		parallelDao = new CoursesParallelDao(semesterCode);
	}

	public void saveFullExportToDb() {
		ACTIVITY_TYPES.values().forEach(t -> entityManager.merge(t));

		final Set<Department> departments = fetchDepartments();
		departments.forEach(d -> entityManager.merge(d));

		final Collection<Location> locations = fetchLocations();
		locations.forEach(l -> entityManager.merge(l));

		final Collection<Suitability> locSuitabs = fetchFITLocationSuitabilities(locations);
		locSuitabs.forEach(s -> entityManager.merge(s));


		final Collection<Module> modules = fetchModulesStoreTeacherSuitabilities(
				departments,
				fetchCoursesDto()
//				.stream().filter(c -> c.getCode().equals("AKCE")).collect(Collectors.toSet())
		);
		modules.forEach(m -> entityManager.merge(m));

		final Collection<Activity> activities = fetchActivities(
				modules.stream()
//                            .filter(m -> m.getCode().equals("AKCE"))
						.collect(Collectors.toSet()),
				locations
		);
		activities.forEach(a -> entityManager.merge(a));
	}

	private Set<Activity> fetchActivities(final Collection<Module> modules,
	                                      final Collection<Location> locations) {
		final Set<Activity> result = new HashSet<>();
		for (final Module m : modules) {
			for (final Parallel parDto : parallelDao.getByCode(m.getCode())) {
				if (parDto.getTtSlots() != null) {
					for (final var tsDto : parDto.getTtSlots()) {
						final Activity a = new Activity(m);
						final TimetableSlot tsDb = new TimetableSlot(
								tsDto.getDay() == null ? null : DayOfWeek.of(tsDto.getDay()),
								tsDto.getFirstHour(),
								tsDto.getDuration(),
								weekAvailabilities.get(tsDto.getParity()));

						if (tsDto.getRoomCode() != null)
							tsDb.setRoom(locations.stream().filter(l -> l.getCode().equals(tsDto.getRoomCode())).findAny().get());

						a.setActivityType(ACTIVITY_TYPES.get(parDto.getParalleltype()));
						a.setKosId(Long.valueOf(tsDto.getKosId()));
						a.setName(m.getCode() + '/' + a.getActivityType().getId()
								          + '/' + parDto.getCode().toString() + '_'
								          + tsDb.getParity().getId());
						a.setPlannedSize(parDto.getCapacity());
						if (a.getActivityType().equals(LECTURE_ACTIVITY_TYPE)) {
							a.setSuitability(m.getSuitabilityLecture());
						} else if (a.getActivityType().equals(SEMINAR_ACTIVITY_TYPE)
								           || a.getActivityType().equals(LABORATORY_ACTIVITY_TYPE)) {
							a.setSuitability(m.getSuitabilityTutorial());
						}
						final List<String> teacherUsernames = parDto.getTeacherUsernames();
						if (teacherUsernames != null && teacherUsernames.size() >= 1) {
							final Teacher t1add = username2teacher.get(teacherUsernames.get(0));
							if (t1add != null)
								a.setTeacher1(t1add);
							if (teacherUsernames.size() >= 2) {
								final Teacher t2add = username2teacher.get(teacherUsernames.get(1));
								if (t2add != null) {
									a.setTeacher2(t2add);
								}
							}
						}
						a.setTimetableSlot(tsDb);
						result.add(a);
					}
				}
			}
		}
		return result;
	}

	public static Set<Department> fetchDepartments() {
		final DivisionDao divDao = new DivisionDao();
		return divDao.getAll().stream()
				       .map(d -> new Department(Long.valueOf(d.getKosId()), d.getCode(), d.getTitle()))
				       .collect(Collectors.toSet());
	}

	public static Set<Location> fetchLocations() {
		final RoomDao roomDao = new RoomDao();
		return roomDao.getAll().stream()
				       .map(r -> new Location(
						       Long.valueOf(r.getKosId()),
						       r.getCode(),
						       r.getCapacityTeaching()))
				       .collect(Collectors.toSet());
	}

	public Collection<Course> fetchCoursesDto() {
		return courseDao.getAll();
	}

	public Collection<Module> fetchModulesStoreTeacherSuitabilities(
			final Collection<Department> departments,
			final Collection<Course> coursesDto
	) {

		final Collection<Module> result = new HashSet<>();
		final Map<String, Department> code2dept = departments.stream().collect(Collectors.toMap(Department::getCode, Function.identity()));

		for (final Course courseDto : coursesDto) {

			final Department dept
					= code2dept.containsKey(courseDto.getDepartmentCode())
							  ? code2dept.get(courseDto.getDepartmentCode())
							  : null;

			final Module module = new Module(Long.valueOf(courseDto.getKosId()),
					courseDto.getCode(),
					courseDto.getName(),
					courseDto.getTotalCapacity(),
					courseDto.getOccupied(),
					courseDto.getTutorialCapacity(),
					dept,
					weekAvailabilities.get(WEEK_AVAILABILITY_BOTH_LABEL).getPattern());

			final Suitability lecSuit = createTeacherModuleSuitability(
					courseDto.getLecturersLogins(),
					dept,
					module,
					LECTURE_ACTIVITY_TYPE);
			entityManager.merge(lecSuit);

			final Suitability tutSuit = createTeacherModuleSuitability(
					courseDto.getInstructorsLogins(),
					dept,
					module,
					LABORATORY_ACTIVITY_TYPE);
			entityManager.merge(tutSuit);

			module.setSuitabilityLecture(lecSuit);
			module.setSuitabilityTutorial(tutSuit);
			result.add(module);
		}
		return result;
	}

	private Set<Teacher> fetchTeachersOfModule(
			final Collection<String> teachersUN,
			final Department dept
	) {
		final Set<Teacher> teachers = new HashSet<>();

		if (teachersUN != null) {
			for (final String username : teachersUN) {
				try {
					final Teacher teacher;
					if (username2teacher.containsKey(username)) {
						teacher = username2teacher.get(username);
					} else {
						final Person p = teacherDao.getByUsername(username);
						teacher = new Teacher(
								Long.valueOf(p.getKosId()),
								p.getPersonalNumber() == null ? null : Integer.valueOf(p.getPersonalNumber()),
								p.getFirstName() + ' ' + p.getLastName(),
								p.getUsername(),
								p.getEmail(),
								dept);
					}
					teachers.add(teacher);
				} catch (final RuntimeException ex) {
					if (ex.getCause() instanceof IOException || ex.getCause() instanceof UnmarshalException) {
						System.err.println(ex.getCause());
					} else {
						throw ex;
					}
				} catch (final URISyntaxException ex) {
					throw new RuntimeException(ex);
				}
			}
		}
		return teachers;
	}

	private Suitability createTeacherModuleSuitability(
			final Collection<String> teachersUN,
			final Department dept,
			final Module module,
			final ActivityType activityType
	) {
		final Suitability result = new Suitability();
		final Set<Teacher> teachers = fetchTeachersOfModule(teachersUN, dept);
		result.setId(module.getCode() + "/" + activityType.getId());
		result.setModule(module);
		result.setTeachers(teachers);
		return result;
	}

	public static Collection<Suitability> fetchFITLocationSuitabilities(final Collection<Location> locations) {
		final Set<Location> lecRooms = locations.stream()
				                               .filter(r -> r.getCode().equals("T9:111")
						                                            || r.getCode().equals("T9:105")
						                                            || r.getCode().equals("T9:155")
						                                            || r.getCode().equals("T9:107")
						                                            || r.getCode().equals("TH:A-s135")
						                                            || r.getCode().equals("TK:BS"))
				                               .collect(Collectors.toSet());
		final Set<Location> semRooms = locations.stream()
				                               .filter(r -> r.getCode().equals("T9:343")
						                                            || r.getCode().equals("TH:A-942")
						                                            || r.getCode().equals("TH:A-1247"))
				                               .collect(Collectors.toSet());
		final Set<Location> semBBRooms = locations.stream()
				                                 .filter(r -> r.getCode().equals("T9:301")
						                                              || r.getCode().equals("T9:302")
						                                              || r.getCode().equals("T9:346")
						                                              || r.getCode().equals("T9:347")
						                                              || r.getCode().equals("TH:A-1242")
						                                              || r.getCode().equals("TH:A-1342")
						                                              || r.getCode().equals("TH:A-1442"))
				                                 .collect(Collectors.toSet());
		final Set<Location> pcRooms = locations.stream()
				                              .filter(r -> r.getCode().equals("T9:303")
						                                           || r.getCode().equals("T9:348")
						                                           || r.getCode().equals("T9:350")
						                                           || r.getCode().equals("T9:351"))
				                              .collect(Collectors.toSet());
		final Set<Location> netRooms = locations.stream().
				                                                 filter(r -> r.getCode().equals("T9:344"))
				                               .collect(Collectors.toSet());
		final Set<Location> sysRooms = locations.stream()
				                               .filter(r -> r.getCode().equals("T9:345"))
				                               .collect(Collectors.toSet());
		final Set<Location> pc48Rooms = locations.stream()
				                                .filter(r -> r.getCode().equals("T9:349")
						                                             || r.getCode().equals("TK:PU1"))
				                                .collect(Collectors.toSet());
		final Set<Location> hwRooms = locations.stream()
				                              .filter(r -> r.getCode().equals("TH:A-1042")
						                                           || r.getCode().equals("TH:A-1048"))
				                              .collect(Collectors.toSet());
		final Set<Location> appleRooms = locations.stream()
				                                 .filter(r -> r.getCode().equals("TH:A-1142"))
				                                 .collect(Collectors.toSet());

		return Stream.of(new Suitability("room/lec", lecRooms),
				new Suitability("room/sem", semRooms, semBBRooms),
				new Suitability("room/semBB", semBBRooms),
				new Suitability("room/PC", pcRooms),
				new Suitability("room/net", netRooms),
				new Suitability("room/sysPC", sysRooms),
				new Suitability("room/pc48", pc48Rooms),
				new Suitability("room/hw", hwRooms),
				new Suitability("room/apple", appleRooms))
				       .collect(Collectors.toSet());
	}
}
