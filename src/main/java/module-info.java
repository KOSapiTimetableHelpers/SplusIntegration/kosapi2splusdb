module cz.cvut.fit.kosapi2timetabler.exporter_to_db {
    requires cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;
    requires cz.cvut.fit.kosapi2timetabler.timetabler_db;
    requires java.xml.bind;
    requires java.persistence;
    exports cz.cvut.fit.kosapi2timetabler.exporter_to_db;
}
